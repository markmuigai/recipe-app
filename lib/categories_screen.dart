import 'package:flutter/material.dart';
import 'package:meals_app/dummy_data.dart';
import 'dummy_data.dart';
import 'category_item.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /**
     * Return a scaffold widget first if the widget is an entire screen
     */
    return Scaffold(
      appBar: AppBar(
        // const text widget will never have to update when the map rebuilds 
        title: const Text('DeliMeal'),
      ),
      /**
       * Return grid with multiple categories
       * Unlike listview, gridview also aligns items side by side
       * not just above each other
       * Grid view has a builder mode that can be used to build a dynamic
       * amount of items i.e Gridview.builder() 
       */
      body: GridView(
        // Add padding to te grid
        padding: const EdgeInsets.all(15),
        // provide a list of widgets
        // map the list of dummy data and map it into a list of widgets
        children: DUMMY_CATEGORIES.map(
          (categoryData) => CategoryItem(
            categoryData.title,
            categoryData.color
          )
        ).toList(),
        /**
         * Slivers are scrollable areas on the screen 
         * Grid Delegate handles structuring and layout 
         * Define maximum width for each grid item
         */
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          /**
           * Define how items should be sized based on their height and width relation 
           * 300 height for 200 width, or 1.5
           */
          childAspectRatio: 3 / 2,
          // distance between columns and rows
          crossAxisSpacing: 20,
          mainAxisSpacing: 20
        ),
      ),
    );
  }
}