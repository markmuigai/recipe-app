import 'package:flutter/material.dart';

class Category { 
  final String id;
  final String title;
  final Color color;

  /// Each category object in dummy data list cannot be changed
  /// i.e const dummy_data = const []
  /// add const to the constructor since all the properties are final
  /// i.e the properties cannot be changed after the object has been created
  const Category({
    @required this.id,
    @required this.title,
    this.color = Colors.orange,
  });
}