import 'package:flutter/material.dart';

class CategoryItem extends StatelessWidget {
  // Properties 

  final String title;
  final Color color;

  // Constructor to assign values to the properties
  // Positional arguments
  CategoryItem(this.title, this.color);

  @override
  Widget build(BuildContext context) {
    // Build a custom card instead of the default card widget
    return Container(
      // Use const to prevent edgeinsets from being recreated when the app is rebuilt  
      padding: const EdgeInsets.all(15),
      child: Text(
        title, style: Theme.of(context).textTheme.headline1
      ),
      // use for styling
      decoration: BoxDecoration(
        // pass a list of colors and opacity
        gradient: LinearGradient(
          colors: [color.withOpacity(0.7),
          // add a slightly less transparent version of color to the list
          // as the end value for the gradient
          color
          ],
          // control direction of the gradient
          begin: Alignment.topLeft,
          end: Alignment.bottomRight
        ),
        borderRadius: BorderRadius.circular(15) 
      ),
    );
  }
}